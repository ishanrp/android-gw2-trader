package transaction.gw2.com.gw2transactions.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;

import com.github.machinarius.preferencefragment.PreferenceFragment;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import transaction.gw2.com.gw2transactions.R;
import transaction.gw2.com.gw2transactions.api.GuildWars2Service;
import transaction.gw2.com.gw2transactions.models.TokenInfo;

/**
 * Created by ishan on 23-Aug-15.
 */
public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String PREF_AUTH_TOKEN_KEY = "pref_auth_token_key";

    public static final String TAG = SettingsFragment.class.getName();

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.account_preferences);
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(final SharedPreferences sharedPreferences, final String key) {
        final Preference pref = findPreference(key);
        if (key.equalsIgnoreCase(PREF_AUTH_TOKEN_KEY)) {
            GuildWars2Service.buildService(sharedPreferences.getString(key, ""));
            GuildWars2Service.gw2Api.getTokenInfo(new Callback<TokenInfo>() {
                @Override
                public void success(TokenInfo tokenInfo, Response response) {
                    String notPermission = "";
                    for (String perm : getResources().getStringArray(R.array.required_permissions)) {
                        if (!tokenInfo.getPermissions().contains(perm)) {
                            notPermission += perm;
                            break;
                        }
                    }

                    if (!notPermission.isEmpty()) {
                        pref.setSummary("The generated token does not allow access to " + notPermission + ", please add it and retry again.");
                    } else {
                        pref.setSummary("The app was successfully linked with you Guild Wars 2 account using " + tokenInfo.getName() + " token.");
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    pref.setSummary("You entered an invalid token, please try again by copy pasting the full token from the GW2 website into the app");
                }
            });
        }
    }
}
