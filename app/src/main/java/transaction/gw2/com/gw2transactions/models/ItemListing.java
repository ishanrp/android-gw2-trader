package transaction.gw2.com.gw2transactions.models;

import java.util.List;

/**
 * Created by ishan on 22-Aug-15.
 */
public class ItemListing {
        private long id;
        private List<Listing> buys;
        private List<Listing> sells;

        public long getId() {
                return id;
        }

        public void setId(long id) {
                this.id = id;
        }

        public List<Listing> getBuys() {
                return buys;
        }

        public void setBuys(List<Listing> buys) {
                this.buys = buys;
        }

        public List<Listing> getSells() {
                return sells;
        }

        public void setSells(List<Listing> sells) {
                this.sells = sells;
        }
}
