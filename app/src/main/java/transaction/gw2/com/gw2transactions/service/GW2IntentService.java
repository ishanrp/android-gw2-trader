package transaction.gw2.com.gw2transactions.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import transaction.gw2.com.gw2transactions.R;
import transaction.gw2.com.gw2transactions.api.GuildWars2Service;
import transaction.gw2.com.gw2transactions.models.Item;
import transaction.gw2.com.gw2transactions.util.CommonUtil;
import transaction.gw2.com.gw2transactions.util.Constants;

/**
 * Created by ishan on 20-Aug-15.
 */
public class GW2IntentService extends IntentService {

    public static final String TAG = GW2IntentService.class.getName();

    public static final String EXTRA_START_PAGE_NO = "EXTRA_START_PAGE_NO";
    public static final int INTENT_UPDATE_ITEMS = 0;
    public static final String EXTRA_EXECUTE_WITH_INTENT = "EXTRA_EXECUTE_WITH_INTENT";
    public static final int INTENT_EXPORT_ITEMS = 1;
    public static final String EXTRA_JSON_FILE_NAME = "EXTRA_JSON_FILE_NAME";
    public static final int INTENT_LOAD_ITEMS_FROM_JSON = 2;
    public static final String EXTRA_CLASS_TO_IMPORT = "EXTRA_CLASS_TO_IMPORT";

    // Must create a default constructor
    public GW2IntentService() {
        // Used to name the worker thread, important only for debugging.
        super("realm-populater-service");
    }

    @Override
    public void onCreate() {
        super.onCreate(); // if you override onCreate(), make sure to call super().
        // If a Context object is needed, call getApplicationContext() here.
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        switch (intent.getIntExtra(EXTRA_EXECUTE_WITH_INTENT, INTENT_UPDATE_ITEMS)) {
            case INTENT_UPDATE_ITEMS:
                loadRealmWithItemsFromOnline(intent);
                break;
            case INTENT_EXPORT_ITEMS:
                exportRealmToJson(intent);
                break;
            case INTENT_LOAD_ITEMS_FROM_JSON:
                loadJsonFromStream(intent);
        }
    }

    private void exportRealmToJson(Intent intent) {
        Realm realm = Realm.getDefaultInstance();
        String filename = intent.getStringExtra(EXTRA_JSON_FILE_NAME);
        BufferedWriter bufferedWriter = null;
        long exportedCount = 1;
        try {
            File jsonFile = new File(Constants.EXTERNAL_STORAGE_FILE, filename);
            jsonFile.delete();

            bufferedWriter = new BufferedWriter(new FileWriter(jsonFile));
            RealmResults<Item> itemRealmResults = realm.allObjects(Item.class);
            int total = itemRealmResults.size();

            bufferedWriter.write("[");
            for (Item item : itemRealmResults) {
                bufferedWriter.write(Constants.GSON.toJson(item, Item.class));
                if (exportedCount < total) {
                    bufferedWriter.write(",");
                }
                exportedCount++;
            }
            bufferedWriter.write("]");

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            CommonUtil.close(bufferedWriter);
            CommonUtil.close(realm);
        }
        createNotification(002, R.drawable.notification_template_icon_bg, getString(R.string.notification_status_title), exportedCount + " Items Exported");
    }

    private void loadJsonFromStream(Intent intent) {
        // Use streams if you are worried about the size of the JSON whether it was persisted on disk
        // or received from the network.
        Realm realm = Realm.getDefaultInstance();
        String jsonPath = intent.getStringExtra(EXTRA_JSON_FILE_NAME);

        // Open a transaction to store items into the realm
        InputStream stream = null;
        try {
            Class aClass = Class.forName(intent.getStringExtra(EXTRA_CLASS_TO_IMPORT));
            stream = new FileInputStream(new File(Constants.EXTERNAL_STORAGE_FILE, jsonPath));
            realm.beginTransaction();
            realm.allObjects(aClass).clear();
            realm.createAllFromJson(aClass, stream);
            realm.commitTransaction();
        } catch (IOException e) {
            realm.cancelTransaction();
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            CommonUtil.close(stream);
            CommonUtil.close(realm);
        }
        try {
            Realm.compactRealm(realm.getConfiguration());
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        createNotification(002, R.drawable.notification_template_icon_bg, getString(R.string.notification_status_title), "Items Imported");
    }

    private void loadRealmWithItemsFromOnline(Intent intent) {
        Realm realm = Realm.getDefaultInstance();
        int page = intent.getIntExtra(EXTRA_START_PAGE_NO, 0);
        String msg = "Completed!!";
        try {
            for (;; page++) {
                List<Item> items = GuildWars2Service.gw2Api.getItemsDetailsByPage(page);
                realm.beginTransaction();
                realm.copyToRealmOrUpdate(items);
                realm.commitTransaction();
                if (page % 10 == 0) {
                    createNotification(001, R.drawable.notification_template_icon_bg, getString(R.string.notification_status_title), page + " pages completed");
                }
                Thread.sleep(1000);
            }
        } catch (Exception e) {
            msg = e.getMessage();
        } finally {
            if (realm != null) {
                realm.close();
            }
        }

        createNotification(001, R.drawable.notification_template_icon_bg, getString(R.string.notification_status_title), msg);
    }

    private void createNotification(int nId, int iconRes, String title, String body) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this).setSmallIcon(iconRes)
                .setContentTitle(title)
                .setContentText(body)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(body));

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(nId, mBuilder.build());
    }
}
