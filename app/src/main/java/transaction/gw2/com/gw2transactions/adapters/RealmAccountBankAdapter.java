package transaction.gw2.com.gw2transactions.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.RealmBaseAdapter;
import io.realm.RealmResults;
import transaction.gw2.com.gw2transactions.R;
import transaction.gw2.com.gw2transactions.models.BankItem;

/**
 * Created by ishan on 22-Aug-15.
 */
public class RealmAccountBankAdapter extends RealmBaseAdapter<BankItem> implements ListAdapter{
    static class ViewHolder {
        @Bind(R.id.ivItemIcon) ImageView icon;
        @Bind(R.id.tvItemQuantity) TextView quantity;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public RealmAccountBankAdapter(Context context, RealmResults<BankItem> realmResults,
                             boolean automaticUpdate) {
        super(context, realmResults, automaticUpdate);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_icon_with_quantity, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        BankItem bankItem = realmResults.get(position);
        if (bankItem.getItem() != null) {
            viewHolder.quantity.setText(String.valueOf(bankItem.getCount()));
            viewHolder.quantity.setVisibility(View.VISIBLE);
            Picasso.with(context)
                    .load(bankItem.getItem().getIcon())
                    .tag(context)
                    .into(viewHolder.icon);
            viewHolder.icon.setVisibility(View.VISIBLE);
        } else {
            viewHolder.quantity.setVisibility(View.INVISIBLE);
            viewHolder.icon.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }

    public RealmResults<BankItem> getRealmResults() {
        return realmResults;
    }
}
