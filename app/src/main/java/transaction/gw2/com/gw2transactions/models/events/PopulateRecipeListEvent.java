package transaction.gw2.com.gw2transactions.models.events;

import java.util.List;

import transaction.gw2.com.gw2transactions.models.Recipe;

/**
 * Created by ishan on 23-Aug-15.
 */
public class PopulateRecipeListEvent {
    private int type;
    private List<Recipe> recipes;

    public PopulateRecipeListEvent(int type, List<Recipe> recipes) {
        this.type = type;
        this.recipes = recipes;
    }

    public List<Recipe> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
