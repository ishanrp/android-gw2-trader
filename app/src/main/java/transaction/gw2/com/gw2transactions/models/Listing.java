package transaction.gw2.com.gw2transactions.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ishan on 22-Aug-15.
 */
public class Listing {
    private int listings;

    @SerializedName("unit_price")
    private int unitPrice;
    private int quantity;

    public int getListings() {
        return listings;
    }

    public void setListings(int listings) {
        this.listings = listings;
    }

    public int getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(int unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}