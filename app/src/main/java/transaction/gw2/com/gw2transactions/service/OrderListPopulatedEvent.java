package transaction.gw2.com.gw2transactions.service;

import java.util.List;

import transaction.gw2.com.gw2transactions.models.Order;

/**
 * Created by ishan on 22-Aug-15.
 */
public class OrderListPopulatedEvent {
    private int result;
    private List<Order> orders;

    public OrderListPopulatedEvent(int result, List<Order> orders) {
        this.result = result;
        this.orders = orders;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
