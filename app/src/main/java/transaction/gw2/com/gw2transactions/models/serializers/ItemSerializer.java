package transaction.gw2.com.gw2transactions.models.serializers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import transaction.gw2.com.gw2transactions.models.Item;

/**
 * Created by ishan on 21-Aug-15.
 */
public class ItemSerializer implements JsonSerializer<Item> {
    @Override
    public JsonElement serialize(Item src, Type typeOfSrc, JsonSerializationContext context) {
        final JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", src.getId());
        jsonObject.addProperty("name", src.getName());
//        jsonObject.addProperty("vendor_value", src.getVendor_value());
//        jsonObject.addProperty("icon", src.getIcon());
//        jsonObject.addProperty("level", src.getLevel());
//        jsonObject.addProperty("rarity", src.getRarity());
//        jsonObject.addProperty("type", src.getType());
        return jsonObject;
    }
}
