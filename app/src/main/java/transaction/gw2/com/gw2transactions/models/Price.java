package transaction.gw2.com.gw2transactions.models;

import android.view.View;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import transaction.gw2.com.gw2transactions.R;

/**
 * Created by ishan on 24-Aug-15.
 */
public class Price {
    @Bind({R.id.tvPriceGold,R.id.tvPriceSilver,R.id.tvPriceCopper})
    List<TextView> tvPrices;

    public Price(View view) {
        ButterKnife.bind(this, view);
    }

    public List<TextView> getTvPrices() {
        return tvPrices;
    }
}
