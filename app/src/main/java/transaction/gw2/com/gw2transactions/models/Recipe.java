package transaction.gw2.com.gw2transactions.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ishan on 23-Aug-15.
 */
public class Recipe {
    private int id;
    private String type;

    @SerializedName("output_item_id")
    private int outputItemId;

    @SerializedName("output_item_count")
    private int outputItemCount;

    @SerializedName("min_rating")
    private int minRating;
    private List<String> disciplines;
    private List<String> flags;
    private List<Ingredient> ingredients;
    private Item outputItem;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getOutputItemId() {
        return outputItemId;
    }

    public void setOutputItemId(int outputItemId) {
        this.outputItemId = outputItemId;
    }

    public int getOutputItemCount() {
        return outputItemCount;
    }

    public void setOutputItemCount(int outputItemCount) {
        this.outputItemCount = outputItemCount;
    }

    public int getMinRating() {
        return minRating;
    }

    public void setMinRating(int minRating) {
        this.minRating = minRating;
    }

    public List<String> getDisciplines() {
        return disciplines;
    }

    public void setDisciplines(List<String> disciplines) {
        this.disciplines = disciplines;
    }

    public List<String> getFlags() {
        return flags;
    }

    public void setFlags(List<String> flags) {
        this.flags = flags;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public Item getOutputItem() {
        return outputItem;
    }

    public void setOutputItem(Item outputItem) {
        this.outputItem = outputItem;
    }
}