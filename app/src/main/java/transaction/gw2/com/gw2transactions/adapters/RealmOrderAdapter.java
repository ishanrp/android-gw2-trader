package transaction.gw2.com.gw2transactions.adapters;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.RealmBaseAdapter;
import io.realm.RealmResults;
import transaction.gw2.com.gw2transactions.R;
import transaction.gw2.com.gw2transactions.models.Order;
import transaction.gw2.com.gw2transactions.util.CommonUtil;

/**
 * Created by ishan on 20-Aug-15.
 */
public class RealmOrderAdapter extends RealmBaseAdapter<Order> implements ListAdapter {

    static class ViewHolder {
        @Bind(R.id.tvItemName) TextView name;
        @Bind(R.id.ivItemIcon) ImageView icon;
        @Bind(R.id.tvItemQuantity) TextView quantity;
        @Bind(R.id.tvDate) TextView date;

        @Bind({R.id.tvPriceGold,R.id.tvPriceSilver,R.id.tvPriceCopper})
        List<TextView> tvItemPrice;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public RealmOrderAdapter(Context context, RealmResults<Order> realmResults,
                             boolean automaticUpdate) {
        super(context, realmResults, automaticUpdate);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_order_row, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Order order = realmResults.get(position);
        if (order.getItem() != null) {
            viewHolder.name.setText(order.getItem().getName());
            viewHolder.quantity.setText(String.valueOf(order.getQuantity()));

            CommonUtil.splitAndFillPrice(order.getPrice(), viewHolder.tvItemPrice);
            if (order.getPurchased() != null) {
                viewHolder.date.setText(DateUtils.getRelativeTimeSpanString(context, order.getPurchased().getTime(), true));
            } else {
                viewHolder.date.setText(DateUtils.getRelativeTimeSpanString(context, order.getCreated().getTime(), true));
            }
            Picasso.with(context)
                    .load(order.getItem().getIcon())
                    .tag(context)
                    .into(viewHolder.icon);
        } else {
            Toast.makeText(context, order.getItemId() + " was null", Toast.LENGTH_SHORT).show();
        }

        return convertView;
    }

    public RealmResults<Order> getRealmResults() {
        return realmResults;
    }
}
