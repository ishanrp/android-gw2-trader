package transaction.gw2.com.gw2transactions.models;

/**
 * Created by ishan on 25-Aug-15.
 */
public enum ACTIONS_MULTI_PAGE {
    BUY(new ACTION[]{ACTION.BUY_CURRENT, ACTION.BUY_HISTORY}),
    SELL(new ACTION[]{ACTION.SELL_CURRENT, ACTION.SELL_HISTORY}),
    RECIPE(new ACTION[]{ACTION.RECIPE_IN, ACTION.RECIPE_OUT});

    private ACTION [] actions;

    ACTIONS_MULTI_PAGE(ACTION[] actions) {
        this.actions = actions;
    }

    public ACTION[] getActions() {
        return actions;
    }
}
