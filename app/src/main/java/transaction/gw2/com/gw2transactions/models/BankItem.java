package transaction.gw2.com.gw2transactions.models;

import com.google.gson.annotations.Expose;

import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ishan on 22-Aug-15.
 */
public class BankItem extends RealmObject {
    @PrimaryKey
    private int slot;
    private long id;

    @Expose(serialize = false)
    private Item item;
    private int count;
    private long skin;

    private int type;

    @Ignore
    private List<Long> upgrades;

    @Ignore
    private List<Long> infusions;

    public BankItem() {
    }

    public BankItem(int slot) {
        this.slot = slot;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public long getSkin() {
        return skin;
    }

    public void setSkin(long skin) {
        this.skin = skin;
    }

    public List<Long> getUpgrades() {
        return upgrades;
    }

    public void setUpgrades(List<Long> upgrades) {
        this.upgrades = upgrades;
    }

    public List<Long> getInfusions() {
        return infusions;
    }

    public void setInfusions(List<Long> infusions) {
        this.infusions = infusions;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
