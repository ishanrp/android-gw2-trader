package transaction.gw2.com.gw2transactions.models.events;

import java.util.List;

import transaction.gw2.com.gw2transactions.models.ACTION;
import transaction.gw2.com.gw2transactions.models.Order;

/**
 * Created by ishan on 24-Aug-15.
 */
public class UpdateTradeOrdersEvent {
    private List<Order> orders;
    private ACTION action;

    public UpdateTradeOrdersEvent(List<Order> orders, ACTION action) {
        this.orders = orders;
        this.action = action;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public ACTION getAction() {
        return action;
    }

    public void setAction(ACTION action) {
        this.action = action;
    }
}
