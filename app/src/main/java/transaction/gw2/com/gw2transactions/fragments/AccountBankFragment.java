package transaction.gw2.com.gw2transactions.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.squareup.otto.Subscribe;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;
import butterknife.OnTextChanged;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import transaction.gw2.com.gw2transactions.R;
import transaction.gw2.com.gw2transactions.fragments.custom.SuperFragment;
import transaction.gw2.com.gw2transactions.adapters.RealmAccountBankAdapter;
import transaction.gw2.com.gw2transactions.api.GuildWars2Service;
import transaction.gw2.com.gw2transactions.models.ACTION;
import transaction.gw2.com.gw2transactions.models.BankItem;
import transaction.gw2.com.gw2transactions.models.Item;
import transaction.gw2.com.gw2transactions.models.Order;
import transaction.gw2.com.gw2transactions.models.events.UpdateBankSlotEvent;
import transaction.gw2.com.gw2transactions.service.BusProvider;
import transaction.gw2.com.gw2transactions.util.CommonUtil;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Callbacks} interface
 * to handle interaction events.
 * Use the {@link AccountBankFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AccountBankFragment extends SuperFragment {

    public static final String TAG = AccountBankFragment.class.getName();
    private static final String ACTION_TYPE = "ACTION_TYPE";

    private Callbacks mListener;
    private ACTION mActionType;
    private RealmAccountBankAdapter realmAccountBankAdapter;

    @Bind(R.id.gvBankItems) GridView mGvAccountBank;
    @Bind(R.id.swipe_container) SwipeRefreshLayout mSwipeContainer;
    private SearchView mSearchView;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AccountBankFragment.
     * @param actionType
     */
    public static AccountBankFragment newInstance(ACTION actionType) {
        AccountBankFragment fragment = new AccountBankFragment();
        Bundle args = new Bundle();
        args.putSerializable(ACTION_TYPE, actionType);
        fragment.setArguments(args);
        return fragment;
    }

    public AccountBankFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        if (getArguments() != null) {
            mActionType = (ACTION) getArguments().getSerializable(ACTION_TYPE);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_bank, container, false);
        ButterKnife.bind(this, rootView);

        populateBankGrid();
        updateAccountBankSlots(mActionType.getId());
        setListeners();

        return rootView;
    }

    @OnItemClick(R.id.gvBankItems)
    public void onItemClick(AdapterView<?> parent, int position) {
        BankItem bankItem = (BankItem) parent.getAdapter().getItem(position);
        if (bankItem.getItem() != null) {
            mListener.onItemClicked(new Order(bankItem.getItem().getId(), 0, bankItem.getCount(), 2));
        }
    }

    @OnItemLongClick(R.id.gvBankItems)
    public boolean onItemLongClick(AdapterView<?> parent, int position) {
        BankItem bankItem = (BankItem) parent.getAdapter().getItem(position);
        if (bankItem.getItem() != null) {
            mListener.onItemLongClicked(bankItem.getId());
        }
        return true;
    }

    @OnTextChanged(R.id.etSearch)
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        RealmQuery<BankItem> realmQuery;
        realmQuery = realm.where(BankItem.class).equalTo("type", mActionType.getId());
        if (!s.toString().isEmpty()) {
            realmQuery = realmQuery.contains("item.name", s.toString(), false);
        }
        realmAccountBankAdapter.updateRealmResults(realmQuery.findAllSorted("slot"));
    }

    private void setListeners() {
        mSwipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateAccountBankSlots(mActionType.getId());
            }
        });

        mSwipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    private void populateBankGrid() {
        RealmResults<BankItem> realmResults = realm.where(BankItem.class).equalTo("type", mActionType.getId()).findAllSorted(mActionType.getExtra1());
        realmAccountBankAdapter = new RealmAccountBankAdapter(getActivity(), realmResults, true);
        mGvAccountBank.setAdapter(realmAccountBankAdapter);
    }

    private void updateAccountBankSlots(final int type) {
        if (type == 2) {
            GuildWars2Service.gw2Api.getAccountBank(new Callback<List<BankItem>>() {
                @Override
                public void success(List<BankItem> bankItems, Response response) {
                    BusProvider.getInstance().post(new UpdateBankSlotEvent(type, bankItems));
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("gw2##########", error.getUrl() + " " + error.getMessage());
                }
            });
        } else if (type == 3) {
            GuildWars2Service.gw2Api.getAccountMaterials(new Callback<List<BankItem>>() {
                @Override
                public void success(List<BankItem> bankItems, Response response) {
                    BusProvider.getInstance().post(new UpdateBankSlotEvent(type, bankItems));
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("gw2##########", error.getUrl() + " " + error.getMessage());
                }
            });
        }
    }

    @Subscribe
    public void updateSlotsInRealm(UpdateBankSlotEvent event) {
        BankItem bankItem;

        Realm realm = Realm.getDefaultInstance();
        int type = event.getType();
        List<BankItem> bankItems = event.getBankItems();

        for (int i = 0, slot = 1000 * type, len = bankItems.size(); i < len; i++, slot++) {
            bankItem = bankItems.get(i);
            if (bankItem == null) {
                bankItem = new BankItem();
                bankItems.set(i, bankItem);
            } else {
                Item item = realm.where(Item.class).equalTo("id", bankItem.getId()).findFirst();
                bankItem.setItem(item);
            }
            bankItem.setSlot(slot);
            bankItem.setType(type);
        }

        realm.beginTransaction();
        realm.copyToRealmOrUpdate(bankItems);
        realm.commitTransaction();

        CommonUtil.close(realm);
        mSwipeContainer.setRefreshing(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.bank_menu, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_bank_search);
        mSearchView = (SearchView) searchItem.getActionView();
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mSearchView.clearFocus();
                mSearchView.setQuery("", false);
                mSearchView.setIconified(true);
                searchItem.collapseActionView();

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                RealmQuery<BankItem> realmQuery;
                realmQuery = realm.where(BankItem.class).equalTo("type", mActionType.getId());
                if (!newText.isEmpty()) {
                    realmQuery = realmQuery.contains("item.name", newText, false);
                }
                realmAccountBankAdapter.updateRealmResults(realmQuery.findAllSorted("slot"));
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_bank_search:
                return true;
        }
        return false;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (Callbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement Callbacks");
        }
    }

    @Override
    public void onDestroyOptionsMenu() {
        super.onDestroyOptionsMenu();
        mSearchView.setOnQueryTextListener(null);
        mSearchView = null;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface Callbacks {
        void onItemClicked(Order order);
        void onItemLongClicked(long itemId);
    }

}



//    @Override
//    public boolean onQueryTextSubmit(String query) {
//        searchView.clearFocus();
//        searchView.setQuery("", false);
//        searchView.setIconified(true);
//        searchItem.collapseActionView();
//
//        return false;
//    }
//
//    @Override
//    public boolean onQueryTextChange(String newText) {
//        RealmQuery<BankItem> realmQuery;
//        realmQuery = realm.where(BankItem.class).equalTo("type", mActionType.getId());
//        if (!newText.isEmpty()) {
//            realmQuery = realmQuery.contains("item.name", newText, false);
//        }
//        realmAccountBankAdapter.updateRealmResults(realmQuery.findAllSorted("slot"));
//        return true;
//    }