package transaction.gw2.com.gw2transactions.models.events;

import java.io.Serializable;

import transaction.gw2.com.gw2transactions.models.Item;

/**
 * Created by ishan on 23-Aug-15.
 */
public class ShowItemListingEvent implements Serializable {
    private static final long serialVersionUID = 34234L;

    private long itemId;
    private Item item;
    private long price;
    private long quantity;
    private int type;

    public ShowItemListingEvent(long itemId, Item item, long price, long quantity, int type) {
        this.itemId = itemId;
        this.item = item;
        this.price = price;
        this.quantity = quantity;
        this.type = type;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
