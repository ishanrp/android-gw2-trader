package transaction.gw2.com.gw2transactions.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import butterknife.OnLongClick;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import transaction.gw2.com.gw2transactions.R;
import transaction.gw2.com.gw2transactions.fragments.custom.SuperFragment;
import transaction.gw2.com.gw2transactions.adapters.ListingAdapter;
import transaction.gw2.com.gw2transactions.api.GuildWars2Service;
import transaction.gw2.com.gw2transactions.models.Item;
import transaction.gw2.com.gw2transactions.models.ItemListing;
import transaction.gw2.com.gw2transactions.models.Order;
import transaction.gw2.com.gw2transactions.models.events.PriceChangeEvent;
import transaction.gw2.com.gw2transactions.service.BusProvider;
import transaction.gw2.com.gw2transactions.util.CommonUtil;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Callbacks} interface
 * to handle interaction events.
 * Use the {@link ItemDetailListingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ItemDetailListingFragment extends SuperFragment {
    private static final String ARG_ORDER = "ARG_ORDER";
    public static final String TAG = ItemDetailListingFragment.class.getName();

    private Order mOrder;
    private Callbacks mListener;
    private long mBuyPrice, mSellPrice, mQuantity;

    private ListingAdapter mBuyListingAdapter,  mSellListingAdapter;

    @Bind({ R.id.tvUBPriceGold, R.id.tvUBPriceSilver, R.id.tvUBPriceCopper})
    List<TextView> mTvUBPrice;
    @Bind({ R.id.tvUSPriceGold, R.id.tvUSPriceSilver, R.id.tvUSPriceCopper})
    List<TextView> mTvUSPrice;
    @Bind({ R.id.tvTBPriceGold, R.id.tvTBPriceSilver, R.id.tvTBPriceCopper})
    List<TextView> mTvTBPrice;
    @Bind({ R.id.tvTSPriceGold, R.id.tvTSPriceSilver, R.id.tvTSPriceCopper})
    List<TextView> mTvTSPrice;
    @Bind({ R.id.tvUPPriceGold, R.id.tvUPPriceSilver, R.id.tvUPPriceCopper})
    List<TextView> mTvUPPrice;
    @Bind({ R.id.tvTPPriceGold, R.id.tvTPPriceSilver, R.id.tvTPPriceCopper})
    List<TextView> mTvTPPrice;

    @Bind(R.id.lvBuyOrders) ListView mLvBuyOrders;
    @Bind(R.id.lvsellListings) ListView mLvSellOrders;
    @Bind(R.id.ivItemIcon) ImageView imageView;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param order Parameter 1.
     * @return A new instance of fragment ItemDetailListingFragment.
     */
    public static ItemDetailListingFragment newInstance(Order order) {
        ItemDetailListingFragment fragment = new ItemDetailListingFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_ORDER, order);
        fragment.setArguments(args);
        return fragment;
    }

    public ItemDetailListingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mOrder = (Order) getArguments().getSerializable(ARG_ORDER);
            mOrder.setItem(realm.where(Item.class).equalTo("id", mOrder.getItemId()).findFirst());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_order_item_detail, container, false);
        ButterKnife.bind(this, rootView);

        TextView mTvQuantity = ((TextView) rootView.findViewById(R.id.tvItemQuantity));
        TextView tvItemName = ((TextView) rootView.findViewById(R.id.tvItemName));

        initData();

        tvItemName.setText(mOrder.getItem().getName());
        mTvQuantity.setText(String.valueOf(mQuantity));

        Picasso.with(getActivity())
                .load(mOrder.getItem().getIcon())
                .tag(getActivity())
                .into(imageView);

        calculatePrices(new PriceChangeEvent());
        populateData(mOrder.getType());
        setListeners();

        return rootView;
    }


    public void initData() {
        if (mOrder.getType() < 2) {
            RealmResults<Order> orderRealmResults = realm.where(Order.class)
                    .equalTo("itemId", mOrder.getItemId())
                    .equalTo("type", mOrder.getType()).findAll();

            mQuantity = orderRealmResults.sum("quantity").longValue();

            if (mOrder.getType() == 0) {
                mBuyPrice = mOrder.getPrice();
            } else if (mOrder.getType() == 1) {
                mSellPrice = mOrder.getPrice();
            }
        } else mQuantity = mOrder.getQuantity();
    }

    public void populateData(final int type) {
        GuildWars2Service.gw2Api.getCommerceListings(mOrder.getItem().getId(), new Callback<ItemListing>() {
            @Override
            public void success(ItemListing itemListing, Response response) {
                BusProvider.getInstance().post(itemListing);

                mBuyPrice = mBuyPrice == 0 && !itemListing.getBuys().isEmpty() ? itemListing.getBuys().get(0).getUnitPrice() : mBuyPrice;
                mSellPrice = mSellPrice == 0 && !itemListing.getSells().isEmpty() ? itemListing.getSells().get(0).getUnitPrice() : mSellPrice;
                BusProvider.getInstance().post(new PriceChangeEvent());
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("gw2##########", error.getUrl() + " " + error.getMessage());
            }
        });
    }

    @OnItemClick(R.id.lvBuyOrders)
    public void onBuyOrderItemClick(AdapterView<?> parent, View view, int position, long id) {
        mBuyPrice = mBuyListingAdapter.getItem(position).getUnitPrice();
        BusProvider.getInstance().post(new PriceChangeEvent());
    }

    @OnItemClick(R.id.lvsellListings)
    public void onSellOrderItemClick(AdapterView<?> parent, View view, int position, long id) {
        mSellPrice = mSellListingAdapter.getItem(position).getUnitPrice();
        BusProvider.getInstance().post(new PriceChangeEvent());
    }

    @OnLongClick(R.id.ivItemIcon)
    public boolean onLongClick(View v) {
        mListener.onItemLongClicked(mOrder.getItemId());
        return true;
    }

    public void setListeners() {
        imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mListener.onItemLongClicked(mOrder.getItemId());
                return true;
            }
        });
    }

    @Subscribe
    public void calculatePrices(PriceChangeEvent event) {
        CommonUtil.splitAndFillPrice(mBuyPrice, mTvUBPrice);
        CommonUtil.splitAndFillPrice(mBuyPrice * mQuantity, mTvTBPrice);

        CommonUtil.splitAndFillPrice(mSellPrice, mTvUSPrice);
        CommonUtil.splitAndFillPrice(mSellPrice * mQuantity, mTvTSPrice);

        long unitProfit = (long) ((mSellPrice * 0.85) - mBuyPrice);

        CommonUtil.splitAndFillPrice(unitProfit, mTvUPPrice);
        CommonUtil.splitAndFillPrice(unitProfit * mQuantity, mTvTPPrice);
    }

    @Subscribe
    public void fillListings(ItemListing itemListing) {
        mBuyListingAdapter = new ListingAdapter(getActivity(), itemListing.getBuys());
        mSellListingAdapter = new ListingAdapter(getActivity(), itemListing.getSells());
        mLvBuyOrders.setAdapter(mBuyListingAdapter);
        mLvSellOrders.setAdapter(mSellListingAdapter);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (Callbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement Callbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface Callbacks {
        public void onItemLongClicked(long itemId);
    }
}
