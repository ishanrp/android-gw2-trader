package transaction.gw2.com.gw2transactions.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ishan on 23-Aug-15.
 */
public class Ingredient {
    @SerializedName("item_id")
    private int itemId;
    private int count;

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
