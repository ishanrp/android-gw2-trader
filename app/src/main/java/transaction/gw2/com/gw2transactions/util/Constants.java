package transaction.gw2.com.gw2transactions.util;

import android.os.Environment;
import android.support.annotation.NonNull;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;

import io.realm.RealmObject;
import transaction.gw2.com.gw2transactions.models.serializers.ItemSerializer;

/**
 * Created by ishan on 21-Aug-15.
 */
public class Constants {
    public static final String EXTERNAL_STORAGE_PATH = Environment.getExternalStorageDirectory().getPath() + File.separator + "aGW2";
    public static final File EXTERNAL_STORAGE_FILE = new File(Environment.getExternalStorageDirectory().getPath() + File.separator + "aGW2");
    public static final Gson GSON = getGson();

    @NonNull
    private static Gson getGson() {
        try {
            return new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                    .registerTypeAdapter(Class.forName("transaction.gw2.com.gw2transactions.models.Item"), new ItemSerializer())
                    .setExclusionStrategies(new ExclusionStrategy() {
                        @Override
                        public boolean shouldSkipField(FieldAttributes f) {
                            return f.getDeclaringClass().equals(RealmObject.class);
                        }

                        @Override
                        public boolean shouldSkipClass(Class<?> clazz) {
                            return false;
                        }
                    })
                    .create();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
