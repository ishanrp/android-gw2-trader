package transaction.gw2.com.gw2transactions.models.events;

import java.util.List;

import transaction.gw2.com.gw2transactions.models.BankItem;

/**
 * Created by ishan on 23-Aug-15.
 */
public class UpdateBankSlotEvent {
    private int type;
    private List<BankItem> bankItems;

    public UpdateBankSlotEvent(int type, List<BankItem> bankItems) {
        this.type = type;
        this.bankItems = bankItems;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public List<BankItem> getBankItems() {
        return bankItems;
    }

    public void setBankItems(List<BankItem> bankItems) {
        this.bankItems = bankItems;
    }
}
