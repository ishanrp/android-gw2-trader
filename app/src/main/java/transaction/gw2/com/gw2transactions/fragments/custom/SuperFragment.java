package transaction.gw2.com.gw2transactions.fragments.custom;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.squareup.leakcanary.RefWatcher;

import butterknife.ButterKnife;
import io.realm.Realm;
import transaction.gw2.com.gw2transactions.GW2Application;
import transaction.gw2.com.gw2transactions.service.BusProvider;
import transaction.gw2.com.gw2transactions.util.CommonUtil;

/**
 * Created by ishan on 24-Aug-15.
 */
public class SuperFragment extends Fragment {
    protected Realm realm;
    public static final String SAVED_IS_HIDDEN = "SAVED_IS_HIDDEN";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        realm = Realm.getDefaultInstance();

        if (savedInstanceState != null) {
            if (savedInstanceState.getBoolean(SAVED_IS_HIDDEN, false)) {
                getFragmentManager().beginTransaction().hide(this).commit();
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(SAVED_IS_HIDDEN, isHidden());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        CommonUtil.close(realm);
        RefWatcher refWatcher = GW2Application.getRefWatcher(getActivity());
        refWatcher.watch(this);
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }
}
