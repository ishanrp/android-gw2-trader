package transaction.gw2.com.gw2transactions.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ogaclejapan.smarttablayout.SmartTabLayout;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import transaction.gw2.com.gw2transactions.R;
import transaction.gw2.com.gw2transactions.adapters.SlidingPageFragmentAdapter;
import transaction.gw2.com.gw2transactions.fragments.custom.SuperFragment;
import transaction.gw2.com.gw2transactions.models.ACTIONS_MULTI_PAGE;

/**
 * Created by ishan on 24-Aug-15.
 */
public class SlidingFragmentContainer extends SuperFragment {

    private static final String ACTION_TYPE = "ACTION_TYPE";

    private ACTIONS_MULTI_PAGE mAction;

    @Nullable
    @Bind(R.id.view_pager)
    ViewPager viewPager;

    @Nullable
    @Bind({R.id.tvLeftHeader, R.id.tvRightHeader})
    List<TextView> headers;

    private SlidingPageFragmentAdapter adapter;

    public static SlidingFragmentContainer newInstance(ACTIONS_MULTI_PAGE actionType) {
        SlidingFragmentContainer fragment = new SlidingFragmentContainer();
        Bundle args = new Bundle();
        args.putSerializable(ACTION_TYPE, actionType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mAction = (ACTIONS_MULTI_PAGE) getArguments().getSerializable(ACTION_TYPE);
            adapter = new SlidingPageFragmentAdapter(getChildFragmentManager(), mAction);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_view_pager, container, false);
        ButterKnife.bind(this, rootView);

        if (viewPager != null) {
            viewPager.setAdapter(adapter);
            // Give the TabLayout the ViewPager
            SmartTabLayout tabLayout = (SmartTabLayout) rootView.findViewById(R.id.viewpagertab);
            tabLayout.setViewPager(viewPager);
        } else {
            getChildFragmentManager().beginTransaction().replace(R.id.page1, adapter.getItem(0)).replace(R.id.page2, adapter.getItem(1)).commit();
            headers.get(0).setText(adapter.getPageTitle(0));
            headers.get(1).setText(adapter.getPageTitle(1));
        }

        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();

        if (viewPager != null) {
            viewPager.setAdapter(null);
        }
    }
}
