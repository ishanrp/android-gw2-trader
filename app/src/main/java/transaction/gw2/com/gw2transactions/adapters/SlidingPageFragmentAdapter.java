package transaction.gw2.com.gw2transactions.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import hugo.weaving.DebugLog;
import transaction.gw2.com.gw2transactions.fragments.OrderListFragment;
import transaction.gw2.com.gw2transactions.models.ACTIONS_MULTI_PAGE;

/**
 * Created by ishan on 24-Aug-15.
 */
public class SlidingPageFragmentAdapter extends FragmentPagerAdapter {
    ACTIONS_MULTI_PAGE mAction;
    int count;

    public SlidingPageFragmentAdapter(FragmentManager fm, ACTIONS_MULTI_PAGE action) {
        super(fm);
        this.mAction = action;
        this.count = action.getActions().length;
    }

    @Override
    public Fragment getItem(int position) {
        switch (mAction) {
            case BUY:
            case SELL:
                return OrderListFragment.newInstance(mAction.getActions()[position]);
        }
        return null;
    }

    @Override
    public int getCount() {
        return count;
    }

    @DebugLog
    @Override
    public CharSequence getPageTitle(int position) {
        switch (mAction) {
            case BUY:
            case SELL:
                return mAction.getActions()[position].getTitle();
        }
        return "";
    }
}
