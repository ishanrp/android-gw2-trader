package transaction.gw2.com.gw2transactions.models.events;

import java.util.List;

/**
 * Created by ishan on 23-Aug-15.
 */
public class RetrieveRecipeDetailsEvent {
    private List<Long> ids;
    private int type;

    public RetrieveRecipeDetailsEvent(List<Long> ids, int type) {
        this.ids = ids;
        this.type = type;
    }

    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
