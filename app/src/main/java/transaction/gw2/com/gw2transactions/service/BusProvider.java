package transaction.gw2.com.gw2transactions.service;

import com.squareup.otto.Bus;

/**
 * Created by ishan on 22-Aug-15.
 */
public class BusProvider {
    private static final Bus BUS = new Bus();

    public static Bus getInstance() {
        return BUS;
    }

    private BusProvider() {
        // No instances.
    }
}
