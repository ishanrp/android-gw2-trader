package transaction.gw2.com.gw2transactions.api;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.realm.RealmObject;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import transaction.gw2.com.gw2transactions.util.CommonUtil;
import transaction.gw2.com.gw2transactions.util.Constants;

/**
 * Created by ishan on 20-Aug-15.
 */
public class GuildWars2Service {
    public static GuildWars2 gw2Api;

    public static void buildService(final String authToken) {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Authorization", "Bearer " + authToken);
            }
        };

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://api.guildwars2.com")
                .setConverter(new GsonConverter(Constants.GSON))
                .setRequestInterceptor(requestInterceptor)
                .build();

        gw2Api = restAdapter.create(GuildWars2.class);
    }
}
