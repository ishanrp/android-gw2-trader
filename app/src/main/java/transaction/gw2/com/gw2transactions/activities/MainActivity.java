package transaction.gw2.com.gw2transactions.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Realm;
import transaction.gw2.com.gw2transactions.R;
import transaction.gw2.com.gw2transactions.fragments.AccountBankFragment;
import transaction.gw2.com.gw2transactions.fragments.ItemDetailListingFragment;
import transaction.gw2.com.gw2transactions.fragments.OrderListFragment;
import transaction.gw2.com.gw2transactions.fragments.RecipesListFragment;
import transaction.gw2.com.gw2transactions.fragments.SettingsFragment;
import transaction.gw2.com.gw2transactions.fragments.SlidingFragmentContainer;
import transaction.gw2.com.gw2transactions.models.ACTION;
import transaction.gw2.com.gw2transactions.models.ACTIONS_MULTI_PAGE;
import transaction.gw2.com.gw2transactions.models.Item;
import transaction.gw2.com.gw2transactions.models.Order;
import transaction.gw2.com.gw2transactions.service.GW2IntentService;

public class MainActivity extends AppCompatActivity
implements OrderListFragment.Callbacks, ItemDetailListingFragment.Callbacks, AccountBankFragment.Callbacks, RecipesListFragment.Callbacks {

    public static String TAG_VISIBLE_FRAGMENT = "TAG_VISIBLE_FRAGMENT";
    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
//    private boolean mTwoPane;
    @Bind(R.id.drawer_layout) DrawerLayout mDrawer;
    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.nvView) NavigationView mNvDrawer;
    ActionBarDrawerToggle mDrawerToggle;
    private boolean isDrawerLocked = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.main_content_layout);
        if(((ViewGroup.MarginLayoutParams)linearLayout.getLayoutParams()).leftMargin == (int)getResources().getDimension(R.dimen.drawer_size)) {
            mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN, mNvDrawer);
            mDrawer.setScrimColor(Color.TRANSPARENT);
            isDrawerLocked = true;
        }

        mDrawerToggle = setupDrawerToggle();

        if(!isDrawerLocked) {
            mDrawer.setDrawerListener(mDrawerToggle);
            final ActionBar ab = getSupportActionBar();
            ab.setHomeAsUpIndicator(R.drawable.ic_menu);
            ab.setDisplayHomeAsUpEnabled(true);
        }

        setListeners();
        initRealmDatabase();
    }

    private void setListeners() {
        mNvDrawer.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                selectDrawerItem(menuItem);
                return true;
            }
        });
    }

    private void initRealmDatabase() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if(!prefs.getBoolean("firstTime", false)) {

            Intent intent = new Intent(this, GW2IntentService.class);

            intent.putExtra(GW2IntentService.EXTRA_EXECUTE_WITH_INTENT, GW2IntentService.INTENT_LOAD_ITEMS_FROM_JSON);
            intent.putExtra(GW2IntentService.EXTRA_JSON_FILE_NAME, "items.json");
            intent.putExtra(GW2IntentService.EXTRA_CLASS_TO_IMPORT, "transaction.gw2.com.gw2transactions.models.Item");

            startService(intent);

            // run your one time code
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("firstTime", true);
            editor.apply();
        }
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the planet to show based on
        // position
        Fragment fragment = null;

        switch(menuItem.getItemId()) {
            case R.id.nav_buy_fragment:
//                fragment = OrderListFragment.newInstance(ACTION.BUY_CURRENT);
                fragment = SlidingFragmentContainer.newInstance(ACTIONS_MULTI_PAGE.BUY);
                break;
            case R.id.nav_sell_fragment:
//                fragment = OrderListFragment.newInstance(ACTION.SELL_CURRENT);
                fragment = SlidingFragmentContainer.newInstance(ACTIONS_MULTI_PAGE.SELL);
                break;
            case R.id.nav_acc_bank_fragment:
                fragment = AccountBankFragment.newInstance(ACTION.ACCOUNT_BANK);
                break;
            case R.id.nav_material_bank_fragment:
                fragment = AccountBankFragment.newInstance(ACTION.MATERIAL_BANK);
                break;
            case R.id.nav_settings_fragment:
                fragment = SettingsFragment.newInstance();
                break;
            default:
                fragment = SlidingFragmentContainer.newInstance(ACTIONS_MULTI_PAGE.BUY);

        }
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.order_detail_container, fragment, MainActivity.TAG_VISIBLE_FRAGMENT)
                .commit();

        // Highlight the selected item, update the title, and close the drawer
        menuItem.setChecked(true);
        setTitle(menuItem.getTitle());

        if (!isDrawerLocked) {
            mDrawer.closeDrawers();
        }
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open,  R.string.drawer_close);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        Intent intent;
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.menu_update_db:
                Realm realm = Realm.getDefaultInstance();
                int pageNo = realm.allObjects(Item.class).size() / 50;
                intent = new Intent(this, GW2IntentService.class);
                intent.putExtra(GW2IntentService.EXTRA_START_PAGE_NO, pageNo - 10);
                intent.putExtra(GW2IntentService.EXTRA_EXECUTE_WITH_INTENT, GW2IntentService.INTENT_UPDATE_ITEMS);
                startService(intent);
                return true;

            case R.id.menu_export:
                intent = new Intent(this, GW2IntentService.class);
                intent.putExtra(GW2IntentService.EXTRA_EXECUTE_WITH_INTENT, GW2IntentService.INTENT_EXPORT_ITEMS);
                intent.putExtra(GW2IntentService.EXTRA_JSON_FILE_NAME, "items_1.json");
                startService(intent);
                return true;

            case R.id.menu_import:
                intent = new Intent(this, GW2IntentService.class);
                intent.putExtra(GW2IntentService.EXTRA_EXECUTE_WITH_INTENT, GW2IntentService.INTENT_LOAD_ITEMS_FROM_JSON);
                intent.putExtra(GW2IntentService.EXTRA_JSON_FILE_NAME, "items.json");
                intent.putExtra(GW2IntentService.EXTRA_CLASS_TO_IMPORT, "transaction.gw2.com.gw2transactions.models.Item");
                startService(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        if (!isDrawerLocked) {
            mDrawerToggle.syncState();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles

        if (mDrawerToggle != null) {
            mDrawerToggle.onConfigurationChanged(newConfig);
        }
    }

    @Override
    public void onItemClicked(Order order) {
        ItemDetailListingFragment fragment = ItemDetailListingFragment.newInstance(order);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.order_detail_container, fragment, TAG_VISIBLE_FRAGMENT)
                .hide(getSupportFragmentManager().findFragmentByTag(TAG_VISIBLE_FRAGMENT))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onItemLongClicked(long itemId) {
        RecipesListFragment fragment = RecipesListFragment.newInstance(ACTION.RECIPE_IN, itemId);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.order_detail_container, fragment, TAG_VISIBLE_FRAGMENT)
                .hide(getSupportFragmentManager().findFragmentByTag(TAG_VISIBLE_FRAGMENT))
                .addToBackStack(null)
                .commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }
}


//        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

//        if (findViewById(R.id.order_detail_container) != null) {
//            // The detail container view will be present only in the
//            // large-screen layouts (res/values-large and
//            // res/values-sw600dp). If this view is present, then the
//            // activity should be in two-pane mode.
//            mTwoPane = true;
//        }

//        if (savedInstanceState == null) {
//            NavigationActionFragment fragment = (NavigationActionFragment) getSupportFragmentManager().findFragmentByTag(NavigationActionFragment.TAG);
//            if (fragment == null) {
//                fragment = new NavigationActionFragment();
//            }
//            getSupportFragmentManager().beginTransaction()
//                    .replace(R.id.order_list_container, fragment)
//                    .commit();
//        }



//    /**
//     * Callback method from {@link NavigationActionFragment.Callbacks}
//     * indicating that the item with the given ID was selected.
//     * @param actionType
//     */
//    @Override
//    public void onItemSelected(ACTION actionType) {
//        if (mTwoPane) {
//            // In two-pane mode, show the detail view in this activity by
//            // adding or replacing the detail fragment using a
//            // fragment transaction.
//            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//            Fragment fragment;
//            switch (actionType.getId()) {
//                case 0:
//                case 1:
//                    fragment = OrderListFragment.newInstance(actionType);
//                    break;
//                case 2:
//                case 3:
//                    fragment = AccountBankFragment.newInstance(actionType);
//                    break;
//                case 4:
//                    fragment = RecipesListFragment.newInstance(46731L);
//                    break;
//                default:
//                    fragment = SettingsFragment.newInstance();
//            }
//
//            getSupportFragmentManager()
//                    .beginTransaction()
//                    .replace(R.id.order_detail_container, fragment, MainActivity.TAG_VISIBLE_FRAGMENT)
//                    .commit();
//
//        } else {
//            // In single-pane mode, simply start the detail activity
//            // for the selected item ID.
//            Intent detailIntent = new Intent(this, ActionContainerActivity.class);
//            detailIntent.putExtra(OrderListFragment.ACTION_TYPE, actionType);
//            startActivity(detailIntent);
//        }
//    }
//
//    @Override
//    public boolean getPaneMode() {
//        return mTwoPane;
//    }
