package transaction.gw2.com.gw2transactions.util;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;

import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by ishan on 20-Aug-15.
 */
public class CommonUtil {
    public static final String TAG = CommonUtil.class.getName();

    public static long parseLong(String str) {
        long value = 0;

        try {
            value = Long.parseLong(str);
        } catch (NumberFormatException e) {
            Log.e(TAG, e.getMessage());
        }
        return value;
    }

    public static String join(List list) {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0, size = list.size(), lastComma = size - 1; i < size; i++) {
            stringBuilder.append(list.get(i));

            if(i < lastComma)
                stringBuilder.append(",");
        }
        return stringBuilder.toString();
    }

    public static void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    public static String[] splitPrice(String price, boolean isNegative) {
        String []splittedPrice = {"","",""};
        String prepend = isNegative ? "-" : "";
        int priceLen = price.length();
        if (priceLen > 4) {
            splittedPrice[0] = prepend + price.substring(0, priceLen - 4);
            splittedPrice[1] = price.substring(priceLen - 4, price.length() - 2);
            splittedPrice[2] = price.substring(priceLen - 2);
        } else if (priceLen > 2) {
            splittedPrice[1] = prepend + price.substring(0, priceLen - 2);
            splittedPrice[2] = price.substring(priceLen - 2);
        } else {
            splittedPrice[2] = prepend + price;
        }
        return splittedPrice;
    }
    
    public static String[] splitPrice(long price) {
        boolean isNegative = false;

        if (price < 0){
            isNegative = true;
            price *= -1;
        }
        return splitPrice(String.valueOf(price), isNegative);
    }

    public static void splitAndFillPrice(long price, List<TextView> textViews) {
        String []splittedPrice = splitPrice(price);
        for (int i = 0; i < 3; i++) {
            if (splittedPrice[i].isEmpty()) {
                textViews.get(i).setVisibility(View.GONE);
            } else {
                textViews.get(i).setText(splittedPrice[i]);
                textViews.get(i).setVisibility(View.VISIBLE);
            }
        }
    }

    public static Header getHeader(Response response, String key) {
        Header header = null;
        for (Header h : response.getHeaders()) {
            if (h.getName().equalsIgnoreCase(key)) {
                header = h;
                break;
            }
        }

        return header;
    }
}
