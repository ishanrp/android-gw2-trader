package transaction.gw2.com.gw2transactions.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import transaction.gw2.com.gw2transactions.R;
import transaction.gw2.com.gw2transactions.models.Listing;
import transaction.gw2.com.gw2transactions.util.CommonUtil;

/**
 * Created by ishan on 22-Aug-15.
 */
public class ListingAdapter extends ArrayAdapter<Listing> {

    private List<Listing> listings;

    static class ViewHolder {
        @Bind(R.id.tvListings) TextView listings;
        @Bind(R.id.tvItemQuantity) TextView quantity;
        @Bind({R.id.tvMarketPriceGold,R.id.tvMarketPriceSilver,R.id.tvMarketPriceCopper})
        List<TextView> tvItemPrice;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public ListingAdapter(Context context, List<Listing> listings) {
        super(context, 0, listings);

        this.listings = listings;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_listing_row, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Listing listing = listings.get(position);
        viewHolder.listings.setText(String.valueOf(listing.getListings()));
        viewHolder.quantity.setText(String.valueOf(listing.getQuantity()));
        CommonUtil.splitAndFillPrice(listing.getUnitPrice(), viewHolder.tvItemPrice);

        return convertView;
    }

}
