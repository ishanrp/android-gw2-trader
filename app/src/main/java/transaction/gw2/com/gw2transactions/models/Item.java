package transaction.gw2.com.gw2transactions.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ishan on 20-Aug-15.
 */
public class Item extends RealmObject {
    @PrimaryKey
    private long id;
    private String name;
    private String icon;
//    private String description;
//    private String type;
//    private int level;
//    private String rarity;
//    private long vendor_value;
//    private List<String> flags;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

//    public String getType() {
//        return type;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }
//
//    public int getLevel() {
//        return level;
//    }
//
//    public void setLevel(int level) {
//        this.level = level;
//    }
//
//    public String getRarity() {
//        return rarity;
//    }
//
//    public void setRarity(String rarity) {
//        this.rarity = rarity;
//    }
//
//    public long getVendor_value() {
//        return vendor_value;
//    }
//
//    public void setVendor_value(long vendor_value) {
//        this.vendor_value = vendor_value;
//    }
}
