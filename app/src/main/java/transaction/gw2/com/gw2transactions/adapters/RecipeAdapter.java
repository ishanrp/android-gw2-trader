package transaction.gw2.com.gw2transactions.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import transaction.gw2.com.gw2transactions.R;
import transaction.gw2.com.gw2transactions.models.Recipe;

/**
 * Created by ishan on 23-Aug-15.
 */
public class RecipeAdapter extends ArrayAdapter<Recipe> {
    private List<Recipe> recipes;
    private final Context context;

    static class ViewHolder {
        @Bind(R.id.tvItemName) TextView name;
        @Bind(R.id.ivItemIcon) ImageView icon;
        @Bind(R.id.tvItemQuantity) TextView quantity;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public RecipeAdapter(Context context, List<Recipe> recipes) {
        super(context, 0, recipes);

        this.context = context;
        this.recipes = recipes;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_order_row, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Recipe recipe = recipes.get(position);
        if (recipe.getOutputItem() != null) {
            viewHolder.name.setText(recipe.getOutputItem().getName());
            viewHolder.quantity.setText(String.valueOf(recipe.getOutputItemCount()));

            Picasso.with(context)
                    .load(recipe.getOutputItem().getIcon())
                    .tag(context)
                    .into(viewHolder.icon);
        } else {
            Toast.makeText(context, recipe.getOutputItemId() + " was null", Toast.LENGTH_SHORT).show();
        }

        return convertView;
    }

    public List<Recipe> getRecipes() {
        return recipes;
    }
}