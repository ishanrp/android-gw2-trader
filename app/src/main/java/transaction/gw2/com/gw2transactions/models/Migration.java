package transaction.gw2.com.gw2transactions.models;

import io.realm.Realm;
import io.realm.RealmMigration;
import io.realm.internal.ColumnType;
import io.realm.internal.Table;

/**
 * Created by ishan on 20-Aug-15.
 */
public class Migration implements RealmMigration {
    @Override
    public long execute(Realm realm, long version) {
        if (version == 0) {
            Table itemTable = realm.getTable(Item.class);
            Table orderTable = realm.getTable(Order.class);
            orderTable.addColumn(ColumnType.INTEGER, "id");
            orderTable.addColumn(ColumnType.INTEGER, "price");
            orderTable.addColumn(ColumnType.INTEGER, "quantity");
            orderTable.addColumn(ColumnType.DATE, "created");
            orderTable.addColumn(ColumnType.DATE, "purchased");
            orderTable.addColumnLink(ColumnType.LINK, "item", itemTable);
            orderTable.setPrimaryKey("id");
            orderTable.addSearchIndex(orderTable.getColumnIndex("id"));

            version++;
        }
        return version;
    }
}