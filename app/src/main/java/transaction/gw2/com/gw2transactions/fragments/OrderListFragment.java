package transaction.gw2.com.gw2transactions.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.squareup.otto.Subscribe;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import io.realm.Realm;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import transaction.gw2.com.gw2transactions.R;
import transaction.gw2.com.gw2transactions.fragments.custom.SuperFragment;
import transaction.gw2.com.gw2transactions.adapters.RealmOrderAdapter;
import transaction.gw2.com.gw2transactions.api.GuildWars2Service;
import transaction.gw2.com.gw2transactions.models.ACTION;
import transaction.gw2.com.gw2transactions.models.Item;
import transaction.gw2.com.gw2transactions.models.Order;
import transaction.gw2.com.gw2transactions.models.events.UpdateTradeOrdersEvent;
import transaction.gw2.com.gw2transactions.service.BusProvider;
import transaction.gw2.com.gw2transactions.util.CommonUtil;

public class OrderListFragment extends SuperFragment implements TabLayout.OnTabSelectedListener {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String TAG = OrderListFragment.class.getName();
    public static final String ACTION_TYPE = "ACTION_TYPE";
    private static final String [] TAB_CONTENT_ARRAY = {"Current", "History"};
    public static final String SAVED_SELECTED_TAB = "SAVED_SELECTED_TAB";

    /**
     * The dummy content this fragment is presenting.
     */
    private ACTION mAction;
    private Callbacks mCallbacks;
    private boolean isCurrent = false;

    @Bind(R.id.order_detail) ListView mListView;
    @Bind(R.id.swipe_container) SwipeRefreshLayout mSwipeContainer;

    public interface Callbacks {
        void onItemClicked(Order order);
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public OrderListFragment() {
    }

    public static OrderListFragment newInstance(ACTION orderType) {
        OrderListFragment fragment = new OrderListFragment();
        Bundle args = new Bundle();
        args.putSerializable(ACTION_TYPE, orderType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        }
        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ACTION_TYPE)) {
            mAction = (ACTION) getArguments().getSerializable(ACTION_TYPE);
            if (mAction == ACTION.BUY_CURRENT || mAction == ACTION.SELL_CURRENT) {
                isCurrent = true;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_order_detail, container, false);
        ButterKnife.bind(this, rootView);

        loadOrdersListView();
        setListener();

        return rootView;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        loadOrdersListView();
    }

    private void loadOrdersListView() {
        checkForOrderUpdates(mAction);
        String sortByField = isCurrent ? "created" : "purchased";
        RealmResults<Order> orderRealmResults = realm.where(Order.class).equalTo("type", mAction.getId()).findAllSorted(sortByField, false);
        mListView.setAdapter(new RealmOrderAdapter(getActivity(), orderRealmResults, true));
    }

    private void checkForOrderUpdates(final ACTION action) {
        GuildWars2Service.gw2Api.getTransaction(action.getTitle(), action.getExtra1(), 0, new Callback<List<Order>>() {
            @Override
            public void success(List<Order> orders, Response response) {
                BusProvider.getInstance().post(new UpdateTradeOrdersEvent(orders, action));
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("gw2##########", error.getUrl() + " " + error.getMessage());
            }
        });
    }

    @Subscribe
    public void updateOrdersInRealm(UpdateTradeOrdersEvent event) {
        Realm realm = Realm.getDefaultInstance();
        int type = event.getAction().getId();
        for (Order order : event.getOrders()) {
            Item item = realm.where(Item.class).equalTo("id", order.getItemId()).findFirst();
            order.setItem(item);
            order.setType(type);
        }

        realm.beginTransaction();
        if (isCurrent) {
            realm.where(Order.class).equalTo("type", type).findAll().clear();
        }
        realm.copyToRealmOrUpdate(event.getOrders());
        realm.commitTransaction();
        CommonUtil.close(realm);
        if (mSwipeContainer != null) {
            mSwipeContainer.setRefreshing(false);
        }
    }

    @OnItemClick(R.id.order_detail)
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Order order = (Order) parent.getAdapter().getItem(position);
        mCallbacks.onItemClicked(new Order(order.getItemId(), order.getPrice(), order.getQuantity(), order.getType()));
    }

    private void setListener() {
        mSwipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                checkForOrderUpdates(mAction);
            }
        });

        mSwipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

}