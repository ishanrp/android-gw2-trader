package transaction.gw2.com.gw2transactions.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by ishan on 21-Aug-15.
 */
public class GW2AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent i) {
        Intent intent = new Intent(context, GW2IntentService.class);
        intent.putExtra(GW2IntentService.EXTRA_EXECUTE_WITH_INTENT, GW2IntentService.INTENT_EXPORT_ITEMS);
        intent.putExtra(GW2IntentService.EXTRA_JSON_FILE_NAME, "items_1.json");
        context.startService(intent);
    }
}
