package transaction.gw2.com.gw2transactions;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.squareup.leakcanary.AndroidExcludedRefs;
import com.squareup.leakcanary.DisplayLeakService;
import com.squareup.leakcanary.ExcludedRefs;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import transaction.gw2.com.gw2transactions.fragments.SettingsFragment;
import transaction.gw2.com.gw2transactions.api.GuildWars2Service;
import transaction.gw2.com.gw2transactions.models.Migration;
import transaction.gw2.com.gw2transactions.util.Constants;

/**
 * Created by ishan on 20-Aug-15.
 */
public class GW2Application extends Application {

    private RefWatcher refWatcher;

    public static RefWatcher getRefWatcher(Context context) {
        GW2Application application = (GW2Application) context.getApplicationContext();
        return application.refWatcher;
    }

    protected RefWatcher installLeakCanary() {
        ExcludedRefs excludedRefs = AndroidExcludedRefs.createAppDefaults()
                .instanceField("android.view.ViewConfiguration", "sConfigurations")
                .build();
        return LeakCanary.install(this, DisplayLeakService.class, excludedRefs);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        refWatcher = installLeakCanary();

        if (Constants.EXTERNAL_STORAGE_FILE.exists()) {
            Constants.EXTERNAL_STORAGE_FILE.mkdirs();
        }

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String authToken = sharedPref.getString(SettingsFragment.PREF_AUTH_TOKEN_KEY, "");

        GuildWars2Service.buildService(authToken);

        RealmConfiguration config = new RealmConfiguration.Builder(Constants.EXTERNAL_STORAGE_FILE)
                .name("default.realm")
                .migration(new Migration())
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(config);
    }
}
