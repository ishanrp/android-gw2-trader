package transaction.gw2.com.gw2transactions.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import transaction.gw2.com.gw2transactions.R;
import transaction.gw2.com.gw2transactions.fragments.custom.SuperFragment;
import transaction.gw2.com.gw2transactions.adapters.RecipeAdapter;
import transaction.gw2.com.gw2transactions.api.GuildWars2Service;
import transaction.gw2.com.gw2transactions.models.ACTION;
import transaction.gw2.com.gw2transactions.models.Item;
import transaction.gw2.com.gw2transactions.models.Recipe;
import transaction.gw2.com.gw2transactions.models.events.PopulateRecipeListEvent;
import transaction.gw2.com.gw2transactions.models.events.RetrieveRecipeDetailsEvent;
import transaction.gw2.com.gw2transactions.service.BusProvider;
import transaction.gw2.com.gw2transactions.util.CommonUtil;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RecipesListFragment.Callbacks} interface
 * to handle interaction events.
 * Use the {@link RecipesListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RecipesListFragment extends SuperFragment {
    private static final String ARG_ACTION = "ARG_ACTION";
    private static final String ARG_ITEM_ID = "ARG_ITEM_ID";
    public static final String TAG = RecipesListFragment.class.getName();

    private Callbacks mListener;
    private long mItemId;
    private ACTION mAction;

    @Bind({R.id.lvRecipesInput, R.id.lvRecipesOutput})
    List<ListView> mLvRecipes;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param action Parameter 1.
     * @return A new instance of fragment RecipesListFragment.
     */
    public static RecipesListFragment newInstance(ACTION action, long itemId) {
        RecipesListFragment fragment = new RecipesListFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_ACTION, action);
        args.putLong(ARG_ITEM_ID, itemId);
        fragment.setArguments(args);
        return fragment;
    }

    public RecipesListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mAction = (ACTION) getArguments().getSerializable(ARG_ACTION);
            mItemId = getArguments().getLong(ARG_ITEM_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recipies, container, false);
        ButterKnife.bind(this, rootView);

        GuildWars2Service.gw2Api.searchRecipes(mItemId, 0, new Callback<List<Long>>() {
            @Override
            public void success(List<Long> ids, Response response) {
                if (!ids.isEmpty())
                    BusProvider.getInstance().post(new RetrieveRecipeDetailsEvent(ids, 0));
                else Toast.makeText(getActivity(), "No input recipe found!!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("gw2##########", error.getUrl() + " " + error.getMessage());
            }
        });

        GuildWars2Service.gw2Api.searchRecipes(0, mItemId, new Callback<List<Long>>() {
            @Override
            public void success(List<Long> ids, Response response) {
                if (!ids.isEmpty())
                    BusProvider.getInstance().post(new RetrieveRecipeDetailsEvent(ids, 1));
                else
                    Toast.makeText(getActivity(), "No output recipe found!!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("gw2##########", error.getUrl() + " " + error.getMessage());
            }
        });

        return rootView;
    }

    @Subscribe
    public void getRecipes(final RetrieveRecipeDetailsEvent event) {
        GuildWars2Service.gw2Api.getRecipes(CommonUtil.join(event.getIds()), new Callback<List<Recipe>>() {
            @Override
            public void success(List<Recipe> recipes, Response response) {
                BusProvider.getInstance().post(new PopulateRecipeListEvent(event.getType(), recipes));
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("gw2##########", error.getUrl() + " " + error.getMessage());
            }
        });
    }

    @Subscribe
    public void populateRecipeList(PopulateRecipeListEvent event) {
        for (Recipe recipe : event.getRecipes()) {
            recipe.setOutputItem(realm.where(Item.class).equalTo("id", recipe.getOutputItemId()).findFirst());
        }
        mLvRecipes.get(event.getType()).setAdapter(new RecipeAdapter(getActivity(), event.getRecipes()));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (Callbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface Callbacks {
    }

}
