package transaction.gw2.com.gw2transactions.models;

import java.io.Serializable;

/**
 * Created by ishan on 20-Aug-15.
 */
public enum ACTION implements Serializable {
    BUY_CURRENT(20, "buys", "current"),
    BUY_HISTORY(21, "buys", "history"),
    SELL_CURRENT(22, "sells", "current"),
    SELL_HISTORY(23, "sells", "history"),
    ACCOUNT_BANK(2, "slot"),
    MATERIAL_BANK(3, "id"),
    RECIPES(4),
    SETTINGS(7),
    RECIPE_IN(10),
    RECIPE_OUT(11);

    private int id;
    private String extra1;
    private String title;

    ACTION(int id) {
        this.id = id;
    }

    ACTION(int id, String extra1) {
        this.id = id;
        this.extra1 = extra1;
    }

    ACTION(int id, String extra1, String title) {
        this.id = id;
        this.extra1 = extra1;
        this.title = title;
    }

    public String getExtra1() {
        return extra1;
    }

    public int getId() {
        return id;
    }

    public static ACTION find(int i) {
        return ACTION.values()[i];
    }

    public String getTitle() {
        return title;
    }
}
