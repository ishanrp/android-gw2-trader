package transaction.gw2.com.gw2transactions.api;

import java.util.List;

import retrofit.Callback;
import retrofit.ResponseCallback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import transaction.gw2.com.gw2transactions.models.BankItem;
import transaction.gw2.com.gw2transactions.models.Item;
import transaction.gw2.com.gw2transactions.models.ItemListing;
import transaction.gw2.com.gw2transactions.models.Listing;
import transaction.gw2.com.gw2transactions.models.Order;
import transaction.gw2.com.gw2transactions.models.Recipe;
import transaction.gw2.com.gw2transactions.models.TokenInfo;

/**
 * Created by ishan on 20-Aug-15.
 */
public interface GuildWars2 {
    @GET("/v2/items")
    List<Long> getItemIdList();

    @GET("/v2/items/{id}")
    Item getItemsDetails(@Path("id") Long id);

    @GET("/v2/items/{id}")
    void getItemsDetails(@Path("id") Long id, Callback<Item> callback);

    @GET("/v2/items")
    List<Item> getItemsDetails(@Query("ids") String ids);

    @GET("/v2/items")
    void getItemsDetails(@Query("ids") String ids, Callback<List<Item>> callback);

    @GET("/v2/items")
    List<Item> getItemsDetailsByPage(@Query("page") int pageNo);

    @GET("/v2/items")
    void getItemsDetailsByPage(@Query("page") int pageNo, Callback<List<Item>> callback);

    @GET("/v2/commerce/transactions/{tense}/{type}")
    List<Order> getTransaction(@Path("tense") String tense, @Path("type") String orderType);

    @GET("/v2/commerce/transactions/{tense}/{type}?page_size=200")
    void getTransaction(@Path("tense") String tense, @Path("type") String orderType, @Query("page") int page, Callback<List<Order>> callback);

    @GET("/v2/commerce/listings/{id}")
    List<ItemListing> getCommerceListings(@Path("id") long id);

    @GET("/v2/commerce/listings/{id}")
    void getCommerceListings(@Path("id") long id, Callback<ItemListing> callback);

    @GET("/v2/commerce/listings")
    List<ItemListing> getCommerceListings(@Query("ids") String ids);

    @GET("/v2/account/bank")
    void getAccountBank(Callback<List<BankItem>> callback);

    @GET("/v2/account/materials")
    void getAccountMaterials(Callback<List<BankItem>> callback);

    @GET("/v2/tokeninfo")
    void getTokenInfo(Callback<TokenInfo> callback);

    @GET("/v2/recipes")
    void getRecipe(@Query("id") long id, Callback<Recipe> callback);

    @GET("/v2/recipes")
    void getRecipes(@Query("ids") String ids, Callback<List<Recipe>> callback);

    @GET("/v2/recipes/search")
    void searchRecipes(@Query("input") long inputId, @Query("output") long outputId, Callback<List<Long>> callback);
}
